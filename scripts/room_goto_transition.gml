///room_goto_transition(room, transition, [steps]);
if (!instance_exists(sys_transition))
{
    with(instance_create(0, 0, sys_transition))
    {
        next_room = argument[0];
        kind = argument[1]; // Kind of Transition
        
        // Change total transition Length
        if (argument_count >= 3 && argument[2] > 0)
        {
            time = argument[2];
        }
    }
}



